# PerfLab: Software Performance Hacking Lab

* [Join on Zoom](https://cuboulder.zoom.us/j/92015108716)

## Spring 2022 schedule

* Jan 26, 11-12:30
* Feb 9, 11-12:30
* Feb 23, 11-12:30
* Mar 9, 11-12:30
* Mar 23, 11-12:30 (spring break?)
* Apr 6, 11-12:30
* Apr 20, 11-12:30
* May 4, 11-12:30

## Purpose

* Share tools and methods for evaluating software performance
* Group hacking sessions to debug and optimize performance of applications and libraries
* Develop and validate performance models
* Learn about the impact of computer architecture on real-world workloads

## How to participate

* Join a meeting
* Reach out on Zulip or create an issue in this repository to
  * share/demo a tool you've found useful
  * volunteer to present on the performance of your workflow
  * share a problem you'd like help with
  * request a demo
  * etc.
* Submit a merge request to this repository with
  * data you'd like to analyze
  * scripts that plot that data
  * etc.

## Resources

* [Jed's CSCI-5576 notes (fall 2019)](https://cu-hpsc.github.io/fall2019/)
* [Brendan Gregg's Flamegraph/Linux `perf`](https://www.brendangregg.com/flamegraphs.html)
